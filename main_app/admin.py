from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import *

# Register your models here.
class CustomUserAdmin(UserAdmin):
    list_display = (
        'username', 'email', 'first_name', 'last_name', 'is_staff', 
        'user_type'
        )

    fieldsets = (
        (None, {
            'fields': ('username', 'password')
        }),
        ('Personal info', {
            'fields': ('first_name', 'last_name', 'email')
        }),
        ('Permissions', {
            'fields': (
                'is_active', 'is_staff', 'is_superuser',
                'groups', 'user_permissions'
            )
        }),
        ('Important dates', {
            'fields': ('last_login', 'date_joined')
        }),
        ('Additional info', {
            'fields': ('user_type',)
        })
    )

    add_fieldsets = (
        (None, {
            'fields': ('username', 'password1', 'password2')
        }),
        ('Personal info', {
            'fields': ('first_name', 'last_name', 'email')
        }),
        ('Permissions', {
            'fields': (
                'is_active', 'is_staff', 'is_superuser',
                'groups', 'user_permissions'
            )
        }),
        ('Important dates', {
            'fields': ('last_login', 'date_joined')
        }),
        ('Additional info', {
            'fields': ('user_type',)
        })
    )

admin.site.register(CustomUser, CustomUserAdmin)
admin.site.register(Profile)
admin.site.register(UserType)
admin.site.register(Consumer)
admin.site.register(Provider)
admin.site.register(Transporter)
admin.site.register(Storage)
admin.site.register(Pollution)
admin.site.register(PollutionProduct)
admin.site.register(PollutionStorage)
admin.site.register(PollutionTransport)
admin.site.register(Resource)
admin.site.register(ResourceProduct)
admin.site.register(ResourceStorage)
admin.site.register(ResourceTransport)
admin.site.register(Product)
admin.site.register(Category)
admin.site.register(Subcategory)
admin.site.register(ProductCategory)
admin.site.register(ProductSubcategory)
admin.site.register(Order)
admin.site.register(OrderItem)
admin.site.register(OrderHistory)
admin.site.register(TransporterBase)
admin.site.register(Fuel)
admin.site.register(Vehicle)
admin.site.register(Transport)