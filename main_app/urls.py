from django.urls import path
from . import views
from main_app import views as main_views

urlpatterns = [
    path('', views.home, name='main_app-home'),
    path('about/', views.about, name='main_app-about'),
    path('t&c/', views.legal, name='main_app-legal'),
    path('storage_register/', main_views.storage_register, name='storage_register'),
    path('storage/', main_views.storage, name='storage'),
    path('storage/<int:pk>/', main_views.storage_detail.as_view(), name='storage_detail'),
    path('storage/storage_edit/<int:storage_id>', main_views.storage_edit, name='storage_edit'),
    path('storage/delete_storage/<int:storage_id>', main_views.delete_storage, name='delete_storage'),
    #path('product_register/<int:storage_id>', main_views.product_register,name='product_register'),
    path('storage/<int:storage_id>/product/', main_views.product_register,name='product_register'),

    path('delete_product/<int:product_id>', main_views.delete_product,name='delete_product'),
    path('product/', main_views.product, name='product'),
    path('product/<int:product_id>', main_views.product_detail, name='product_detail'),
    
    path('add-to-cart/<slug>/', main_views.add_to_cart, name='add-to-cart'),
    path('remove-from-cart/<slug>/', main_views.remove_from_cart, name='remove-from-cart'),
    path('remove_one_item_cart/<slug>/', main_views.remove_one_item_cart, name='remove_one_item_cart'),
    path('checkout/', main_views.CheckoutView.as_view(), name='checkout'),
    path('payment/', main_views.payment, name='payment'),
    path('product/product_edit/<int:product_id>', main_views.product_edit, name='product_edit'),
    path('cart/', main_views.OrderSummaryView.as_view(), name='cart'),
    path('search/', main_views.searchbar, name='search'),
    path('', views.add_to_cart, name='cartadd'),
    path('base_transport/', main_views.base_transport, name='base'),
    path('base_register/', main_views.base_register, name='base_register'),
    path('transporterbase_detail/<int:pk>/', main_views.base_detail.as_view(), name='transporterbase_detail'),
    path('transporterbase/<int:base_id>/vehicle/', main_views.vehicle_register,name='vehicle_register'),
    
    path('transporterbase/base_edit/<int:base_id>', main_views.base_edit, name='base_edit'),
    path('transporterbase/delete_base/<int:base_id>', main_views.delete_base, name='delete_base'),
    path('vehicle/vehicle_edit/<int:vehicle_id>', main_views.vehicle_edit, name='vehicle_edit'),
    path('vehicle/delete_vehicle/<int:vehicle_id>', main_views.delete_vehicle, name='delete_vehicle'),
]
