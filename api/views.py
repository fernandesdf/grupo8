from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
from main_app.models import *
from main_app.serializers import *



#------------------------------------TipoUser------------------------------------#
@api_view(['GET'])
def getUserType(request):
    user_type = UserType.objects.all()
    serializer = UserTypeSerializer(user_type, many=True)
    return Response(serializer.data)


#------------------------------------CustomUser------------------------------------#
@api_view(['GET'])
def getCustomUser(request):
    customer_user = CustomUser.objects.all()
    serializer = CustomUserserializer(customer_user, many=True)
    return Response(serializer.data)



#---------------------------------------Produtos----------------------------------------------------------#

@api_view(['GET'])
def getProducts(request):
    product = Product.objects.all()
    serializer = ProductSerializer(product, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def addProduct(request):
    if request.method == 'POST':
        serializer = ProductSerializer(data=request.data)
        data = {}
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response("erro")

@api_view(['PUT'])
def UpdateProduct(request, slug):
    try:
        product =  Product.objects.get(slug=slug)
    except Product.DoesNotExit:
        return Response('O produto nao existe para ser atualizado')
    
    if request.method == 'PUT':
        serializer = ProductSerializer(product, data=request.data)
        data = {}
        if serializer.is_valid():
            serializer.save()
            data["sucess"] = "Produto foi atualizado com sucesso"
            return Response(data=data)
        return Response(serializer.errors)

@api_view(['DELETE'])
def DeleteProduct(request, slug):
    try:
        product =  Product.objects.get(slug=slug)
    except Product.DoesNotExit:
        return Response('O produto nao existe para ser apagado')
    
    if request.method == 'DELETE':
        operation = product.delete()
        data = {}
        if operation:
            data["sucess"] = "Produto apagado com sucesso"
            return Response(data=data)
        else:
             data["failure"] = "Produto não foi apagado com sucesso"
        return Response(data=data)


#------------------------------------Perfil------------------------------------#

@api_view(['GET'])
def updateProfile(request):
    if request.method == 'GET':
        perfil = ProfileSerializer.objects.all()
        serializer = ProfileSerializer(perfil)
    return Response(serializer.data)


#------------------------------------Poluicao------------------------------------#
@api_view(['GET'])
def getPollution(request):
    if request.method == 'GET':
        poluicao = Pollution.objects.all()
        serializer = PollutionSerializer(poluicao, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def addPollution(request):
    if request.method == 'POST':
        serializer = PollutionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
    #return Response(serializer.data)

@api_view(['DELETE'])
def DeletePollution(request, slug):
    try:
        product =  Pollution.objects.get(name=slug)
    except Pollution.DoesNotExit:
        return Response('A poluicao nao existe para ser apagada')
    
    if request.method == 'DELETE':
        operation = product.delete()
        data = {}
        if operation:
            data["sucess"] = "Poluicao apagada com sucesso"
            return Response(data=data)
        else:
             data["failure"] = "Poluicao não foi apagada com sucesso"
        return Response(data=data)


#------------------------------------Recursos------------------------------------#

@api_view(['GET'])
def getResources(request):
    if request.method == 'GET':
        recursos = Resource.objects.all()
        serializer = ResourceSerializer(recursos, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def addResource(request):
    if request.method == 'POST':
        serializer = ResourceSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['DELETE'])
def DeleteResource(request, slug):
    try:
        resource =  Resource.objects.get(name=slug)
    except Resources.DoesNotExit:
        return Response('O recurso nao existe para ser apagado')
    
    if request.method == 'DELETE':
        operation = resource.delete()
        data = {}
        if operation:
            data["sucess"] = "recurso apagado com sucesso"
            return Response(data=data)
        else:
             data["failure"] = "recurso não foi apagado com sucesso"
        return Response(data=data)


#------------------------------------Categoria------------------------------------#


#------------------------------------SubCategoria------------------------------------#


#------------------------------------Categoria_de_um_Produto------------------------------------#


#------------------------------------SubCategoria_de_um_Produto------------------------------------#