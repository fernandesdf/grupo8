from django.urls import path
from . import views

urlpatterns=[
    path('api', views.getProducts),
    path('api/add_product', views.addProduct),
    path('api/pollution/',views.getPollution,name='poluicao'),
]