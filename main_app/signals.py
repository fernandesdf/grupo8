from django.db.models.signals import post_save
#from django.contrib.auth.models import User # Sender
from django.dispatch import receiver # Receiver
from .models import *

@receiver(post_save, sender=CustomUser)
def create_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
        user_provider = UserType.objects.filter(description="Fornecedor").first()
        user_transporter = UserType.objects.filter(description="Transportador").first()

        if len(CustomUser.objects.filter(username=instance.username,user_type=user_provider.id))==1:
            Provider.objects.create(user=instance)
        elif len(CustomUser.objects.filter(username=instance.username,user_type=user_transporter.id))==1:
            Transporter.objects.create(user=instance)
        else:
            Consumer.objects.create(user=instance)

#@receiver(post_save, sender=CustomUser) # When a user is saved,# the profile is also saved
#def save_profile(sender, instance, **kwargs):
#    instance.profile.save()