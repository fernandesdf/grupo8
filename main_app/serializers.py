from rest_framework import serializers
from .models import *

#_______________________________""""_______________________________
# Users

class UserTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserType
        fields= "__all__"


class CustomUserserializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields= ['password','last_login','is_superuser','username','first_name','last_name','email','is_staff','is_active','date_joined','user_type','groups','user_permissions']


class ProviderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Provider
        fields= ['user']


class ConsumerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Consumer
        fields= ['user']


class TransporterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transporter
        fields= ['user','company']



#_______________________________""""_______________________________
# Profile
class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields= ['user', 'image', 'address', 'cell_phone', 'nif']


#_______________________________""""_______________________________
# Storage

class StorageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Storage
        fields= ['provider', 'address']

#_______________________________""""_______________________________
# Product
class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model= Product
        fields =['title', 'price', 'discount_price', 'description', 'image','storage', 'in_stock']

#_______________________________""""_______________________________
# Category

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields= ['name']

#_______________________________""""_______________________________
# SubCategory

class SubCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Subcategory
        fields= ['category', 'name']

#_______________________________""""_______________________________
# ProductCategory

class ProductCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductCategory
        fields= ['product', 'category']

#_______________________________""""_______________________________
# SubCategory

class  ProductSubcategorySerializer(serializers.ModelSerializer):
    class Meta:
        model =  ProductSubcategory
        fields= ['product', 'subcategory']



#_______________________________""""_______________________________
# Pollution

class PollutionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pollution
        fields = ['name', 'unit']


#_______________________________""""_______________________________
# Resource

class ResourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Resource
        fields = ['name', 'unit']  


#_______________________________""""_______________________________
# Transport

class TransporterBaseSerializer(serializers.ModelSerializer):
    class Meta:
        model =TransporterBase
        fields = ['transporter', 'address']

class FuelSerializer(serializers.ModelSerializer):
    class Meta:
        model =Fuel
        fields = ['name', 'pollution_type','pollution_per_km']

class VehicleSerializer(serializers.ModelSerializer):
    class Meta:
        model =Vehicle
        fields = ['base', 'registration','brand','model','fuel']


class TransportSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transport
        fields = ['transporter', 'start', 'end', 'vehicle']


#_______________________________""""_______________________________
# Pollution related

class PollutionStorageSerializer(serializers.ModelSerializer):
    class Meta:
        model = PollutionStorage
        fields = ['storage', 'pollution', 'generated']


class PollutionProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = PollutionProduct
        fields = ['product', 'pollution', 'generated']


class PollutionTransportSerializer(serializers.ModelSerializer):
    class Meta:
        model = PollutionTransport
        fields = ['transport', 'pollution', 'generated']


#_______________________________""""_______________________________
# Resource related


class ResourceStorageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ResourceStorage
        fields = ['storage', 'resource', 'consumed']



class ResourceProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = ResourceProduct
        fields = ['product', 'resource', 'consumed']



class ResourceTransportSerializer(serializers.ModelSerializer):
    class Meta:
        model = ResourceTransport
        fields = ['transport', 'resource', 'consumed']



#_______________________________""""_______________________________
# Order

class OrderItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderItem
        fields = ['user', 'complete', 'item','amount']

class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = ['user', 'items', 'date_ordered','complete','transaction_id']


class OrderHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderHistory
        fields = ['customer', 'order']